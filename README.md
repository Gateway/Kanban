# Gateway Kanban Board

This is a kanban board for all *short-term* work related to Gateway and Gateway UI.

See the [issue board](https://octo.sh/Gateway/kanban/boards) for details.